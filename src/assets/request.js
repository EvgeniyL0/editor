import users from '../../db.json';

/** Emulate request */
export const authentication = (data) =>
	new Promise((resolve, reject) => {
		const delay = parseInt(Math.random() * 1000);
		let user = users.find(item => item.name === data.name);

		if (user === undefined) {
			return reject('User not found');
		}

		if (user.password !== data.password) {
			return reject('Invalid password');
		}

		setTimeout(() => {
			resolve(user);
		}, delay);
	});
